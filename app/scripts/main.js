$(function () {
  $('.up-btn').handleUpbtn({
    'text': 'До початку сторінки'
  });

  /* accordion */

  !(function() {
    $('.accordion').addClass('accordion--is-ready');
    $('.accordion__head').attr('aria-expanded', false);
    $('.accordion__body').attr('aria-hidden', true);

    $('.accordion').on('click', '.accordion__head', function(e) {
      var expanded = $(e.target).attr('aria-expanded');

      if(expanded === 'true') {
        $(e.target).attr('aria-expanded', false);
        $(e.target).next().attr('aria-hidden', true);
      } else {
        $(e.target).attr('aria-expanded', true);
        $(e.target).next().attr('aria-hidden', false);
      }

      return false;
    });
  })();


  /* language block, accessibility handling closure */

  !(function () {
    $('.lang').focus(function () {
      $(this).addClass('focus');
    }).on('mouseout', function () {
      $(this).removeClass('focus');
    });

    $('.lang__item').focus(function () {
      $('.lang').addClass('focus');
    }).blur(function () {
      $('.lang').removeClass('focus');
    });
  })();

  $('.shops-anchor').click(function () {
    $('html, body').animate({
      scrollTop: $('.shops').offset().top - $('.page-nav').height()
    });
  });

  !(function () {
    var scrollTop = $(window).scrollTop(),
        headerHeight = $('.pageheader').height(),
        $nav = $('.page-nav');

    topmenuHandle.bind(headerHeight);

    $(window).resize(function () {
      headerHeight = $('.pageheader').height();
      topmenuHandle.bind(null, headerHeight);
    })

    $(window).scroll(topmenuHandle.bind(null, headerHeight));

    function topmenuHandle(headerHeight) {
      scrollTop = $(window).scrollTop();

      console.log(headerHeight)

      if(scrollTop > headerHeight) {
        $nav.addClass('active')
      } else {
        $nav.removeClass('active')
      }
    }
  })();


  // AJAX-request for XML-feed

	$.ajax({
		type: "GET",
		url: 'https://r1.shop.kyivstar.ua/ru/public/94cae98d4b9104397a7cad9c039618e6/TPLink.xml',
		dataType: "xml",
		success: function (xml) {
			gadgetsList(xmlParser(xml));
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.error(jqXHR);
			console.error(errorThrown);
		}
	});


  // Shops list handling

  (function shopsListHandler() {
    var listData = Vue.config.KS_CONFIG.serviceCenters;

  	var stores = new Vue({
  		el: '.stores',
  		data: {
  			packages: listData,
  			search: ''
  		},
  		methods: {
  			showMore: function (e) {
  				var $target = $(e.currentTarget),
              li = $('.accordion__head');

  				li.filter(':hidden').addClass('hidden');
  				li.filter(':hidden:lt(10)').slideDown(300);

  				if ($target.hasClass('disabled')) {
  					li.filter('.hidden').slideUp(300);
  					$target.removeClass('d');
  					return;
  				}

  				if (!li.filter(':hidden').length) {
  					$target.addClass('disabled');
  				}
  			}
  		},
  		filters: {
  			sortByTown: function (items) {
  				return items.sort(function (a, b) { return a.town.localeCompare(b.town) });
  			}
  		}
  	});
  })();


  // Accordions

  function gadgetsList(data) {
		// count of elements appear on button "show more" click
		var LIMIT = 4;

		var gadgets = new Vue({
			el: '.gadgets',
			data: {
				gadgets: data,
				limit: LIMIT
			},
			methods: {
				showMore: function (e) {
					this.limit += LIMIT;
					if (this.limit > data.length) {
						$(e.target).addClass('disabled');
					}
				}
			}
		});
	}

	// function for slicing xml CDATA element
	function sliceCDATA(elem) {
		var str = $(elem).html();
		return str.slice(9, (str.length - 3));
	}

	function xmlParser(xml) {
		var arr = [];
		$(xml).find('offer').each(function () {
			arr.push({
				category: $(xml).find('category').attr('id'),
				model: sliceCDATA($(this).find('model')),
				picture: sliceCDATA($(this).find('picture')),
				url: sliceCDATA($(this).find('url')),
				oldPrice: $(this).find('basePrice').text(),
				price: $(this).find('price').text()
			});
		});
		return arr;
	}
});
