// basket.clear();

basket.require({
  url: 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'
})
.then(function () {
  basket.require({
    url: 'https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.js'
  }).then(function () {
    /* https://cdn.kyivstar.ua/sites/default/files/promotions/LANDINGURL/ */
    basket.require({
      // url: 'https://cdn.kyivstar.ua/sites/default/files/promotions/neffos/scripts/main.min.js',
      url: 'scripts/main.min.js',
      skipCache: true
    })
  });
}, function (error) {
  console.log(error);
});
