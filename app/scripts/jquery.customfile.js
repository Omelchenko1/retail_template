!(function () {
  $('.custom-file :file').on('change', function () {
    var fileName = $(this).val().split('\\').pop();

    $(this).closest('.custom-file').find('.custom-file__name').text(fileName);
  });
})();
