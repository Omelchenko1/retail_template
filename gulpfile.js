/**
 *
 *  Web Starter Kit
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

'use strict';

// This gulpfile makes use of new JavaScript features.
// Babel handles this without us having to do anything. It just works.
// You can read more about the new JavaScript features here:
// https://babeljs.io/docs/learn-es2015/

const fs = require('fs'),
      path = require('path'),
      gulp = require('gulp'),
      del = require('del'),
      runSequence = require('run-sequence'),
      bs = require('browser-sync'),
      gulpLoadPlugins = require('gulp-load-plugins'),
      pkg = require('./package.json'),
      critical = require('critical').stream,
      inlinesource = require('gulp-inline-source'),
      gcmq = require('gulp-group-css-media-queries'),

      $ = gulpLoadPlugins(),
      browserSync = bs.create(),
      reload = browserSync.reload;


// Optimize images
gulp.task('images', ['webp'], () =>
  gulp.src(['!app/images/sprite/*', '!app/images/svg/*', 'app/images/**/*.{jpg,png}'])
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
    .pipe(browserSync.stream())
    .pipe($.size({title: 'images'}))
);

// Convert images to WebP
gulp.task('webp', () => {
  return gulp.src(['!app/images/sprite/*', 'app/images/**/*.{jpg,png}'])
    .pipe($.webp({
      quality: 85
    }))
    .pipe(gulp.dest('dist/images'))
    .pipe($.size({title: 'webp'}));
});

// Clean cache
gulp.task('cleanCache', () => {
  return $.cache.clearAll();
});

// Sprite images
gulp.task('sprite', () => {
  var spriteData =
  gulp.src('app/images/sprite/*.*') // raw images for sprite path
  .pipe($.spritesmith({
    imgName: 'sprite.png',
    cssName: '_sprite.scss',
    cssFormat: 'scss',
    padding: 3,
    cssTemplate: 'scss.template.mustache',
    algorithm: 'binary-tree'
  }));

  spriteData.img.pipe(gulp.dest('app/images/')); // path for compiled sprite image
  spriteData.css.pipe(gulp.dest('app/styles/')); // path for compiled sprite variables
});

// Copy fonts to dist
gulp.task('fonts', () => {
  return gulp.src(['app/fonts/**'])
    .pipe(gulp.dest('dist/fonts'))
    .pipe($.size({title: 'fonts'}));
});

// SVG copy
gulp.task('svgCopy', () => {
  return gulp.src(['app/images/svg/**'])
    .pipe(gulp.dest('dist/images/svg'))
    .pipe($.size({title: 'copy SVG'}));
});

// Copy all files at the root level (app)
gulp.task('copy', ['fonts', 'svgCopy'], () =>
  gulp.src([
    'app/*',
    '!app/*.html',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'))
    .pipe($.size({title: 'copy'}))
);

// Compile and automatically prefix stylesheets
gulp.task('styles', () => {
  return gulp.src([
    'app/styles/**/*.scss',
    'app/styles/**/*.css'
  ])
    .pipe($.newer('.tmp/styles'))
    .pipe($.sass({
      precision: 10,
      includePaths: require('node-bourbon').includePaths
    }).on('error', $.sass.logError))
    .pipe($.rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(gcmq())
    .pipe($.if('*.css', $.cssnano({
      discardUnused: false
    })))
    .pipe($.autoprefixer({
      browsers: "last 4 versions"
    }))
    .pipe($.size({title: 'styles'}))
    .pipe(gulp.dest('dist/styles'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('critical', () => {
  return gulp.src('dist/*.html')
    .pipe(critical({
      base: 'dist/',
      src: 'index.html',
      css: ['dist/styles/main.min.css'],
      dest: 'styles/critical.css',
      dimensions: [{
        width: 320,
        height: 480
      },{
        width: 768,
        height: 1024
      },{
        width: 1366,
        height: 960
      }],
      minify: true,
      ignore: ['@font-face']
    }))
    .pipe($.size({title: 'critical'}))
    .pipe(gulp.dest('dist/styles'));
});

// Concatenate and minify JavaScript. Optionally transpiles ES2015 code to ES5.
// to enables ES2015 support remove the line `"only": "gulpfile.babel.js",` in the
// `.babelrc` file.
var scriptsArray = [
  // Note: Since we are not using useref in the scripts build pipeline,
  //       you need to explicitly list your scripts here in the right order
  //       to be correctly concatenated

  // './app/scripts/ismobile.min.js',
  // './app/scripts/slick.min.js',
  // './app/scripts/modaal.min.js',
  // './app/scripts/chosen.jquery.js',
  // './app/scripts/jquery.mCustomScrollbar.concat.min.js',
  // './app/scripts/jquery.mousewheel.min.js',
  // './app/scripts/stickytableheaders.min.js',
  // './app/scripts/jquery.chained.min.js',
  // './app/scripts/jquery.tools.min.tabs.js',
  // './app/scripts/jquery.masked.input.js',
  // './app/scripts/jquery.customfile.js',
  // './app/scripts/vue.config.js',
  './app/scripts/vue.config.ua.js',
  // './app/scripts/fraccordion.min.js',
  // './app/scripts/jquery.collapsible.js',
  './app/scripts/jquery.upbtn.js',
  // './app/scripts/jquery.sticky-kit.min.js',
  './app/scripts/modernizr-webp.js',
  './app/scripts/main.js'
];

// Copy basket.js
gulp.task('copyScripts', () => {
    gulp.src([
      './app/scripts/basket.full.min.js',
      './app/scripts/initbasket.js',
      './app/scripts/loadcss.full.min.js',
      './app/scripts/fontfacesnippet.js',
      './app/scripts/fontfaceobserver.min.js'
    ])
      .pipe($.size({title: 'copyScripts'}))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest('dist/scripts'))
    }
);

// Concatenate all scripts
gulp.task('scripts', ['copyScripts'], () => {
    gulp.src(scriptsArray)
      .pipe($.concat('main.min.js'))
      .pipe($.size({title: 'copyScripts'}))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest('dist/scripts'))
    }
);

// Concatenate & minify all scripts
gulp.task('scriptsProd', ['copyScripts'], () => {
    gulp.src(scriptsArray)
      .pipe($.concat('main.min.js'))
      .pipe($.uglify({preserveComments: 'some'}))
      .pipe($.size({title: 'scriptsProd'}))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest('dist/scripts'))
    }
);

// Scan your HTML for assets & optimize them
gulp.task('html', () => {
  return gulp.src('app/**/*.html')
    .pipe($.useref({
      searchPath: '{.tmp,app}',
      noAssets: true
    }))

    // Minify any HTML
    .pipe($.if('*.html', $.htmlmin({
      removeComments: true,
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      removeAttributeQuotes: true,
      removeRedundantAttributes: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      removeOptionalTags: false
    })))
    // Output files
    .pipe($.if('*.html', $.size({title: 'html', showFiles: true})))
    .pipe(gulp.dest('dist'));
});

// Inline styles and scripts in html-file
gulp.task('inline', () => {
  return gulp.src('dist/*.html')
  .pipe(inlinesource())
  .pipe(gulp.dest('dist/'));
});

// Clean output directory
gulp.task('clean', cb => del(['.tmp', 'dist/*', '!dist/.git'], {dot: true}));

// Clean after build
gulp.task('cleanAfter', cb => del([
  'dist/styles/critical.css',
  'dist/styles/index.css'
], {dot: true}));

// Watch files for changes & reload
gulp.task('serve', ['default'], () => {
  browserSync.init({
    notify: true,
    server: ['.tmp', 'dist'],
    reloadDelay: 800,
    port: 3000,
    ghostMode: false
  });

  gulp.watch(['app/**/*.html'], ['html', reload]);
  gulp.watch(['app/styles/**/*.scss'], ['styles']);
  gulp.watch(['app/scripts/**/*.js'], ['scripts', reload]);
  gulp.watch(['app/images/**/*.{jpg,png}', '!app/images/sprite/*'], ['images']);
});

// Build dev files, the default task
gulp.task('default', ['clean'], cb =>
  runSequence(
    ['cleanCache', 'sprite', 'html', 'scripts', 'images', 'styles'],
    'copy',
    cb
  )
);

// Build production files, the default task
gulp.task('build', ['clean'], cb =>
  runSequence(
    ['cleanCache', 'sprite', 'html', 'scriptsProd', 'images', 'styles'],
    ['copy', 'critical'],
    'inline',
    'cleanAfter',
    cb
  )
);
